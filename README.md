# projet-portfolio-v2

Le but du projet portfolio v2 est de reprendre le portfolio que vous avez fait en premier projet
et d'y rajouter une partie dynamique pour l'affichage et la gestion des projets.
L'idée est d'avoir une petite interface en JS/HTML qui permettra de rajouter des projets à votre
portfolio en indiquant les informations de celui ci (titre, description, image, lien + gitlab).
Cette petite interface sera câblée à une API REST distincte faite en Symfony 4. (avec tout ce que
vous connaissez, les entities, les repository, les controllers avec leur route en mode REST)

Vous allez donc devoir : 
* Créer une nouvelle branche sur votre projet Portfolio sur laquelle vous rajouterai les modifications :
    *  Rajouter une config webpack
    *  Rajouter du Javascript pour faire des requêtes AJAX vers un serveur
    *  Rajouter un bouton d'admin sur le site qui ouvrira une petite pop up (modal bootstrap par exemple)
    *  Créer un formulaire d'ajout de projet dont les informations seront récupérées par le JS pour être envoyée vers l'API REST
    *  Faire que le JS aille chercher les projets sur l'API REST pour les afficher dans le portfolio
* Créer une nouveau projet pour l'API REST (en partant du boilerplate par exemple)
    * Créer l'entité requise (ou les entités si jamais vous avez plus de fonctionnalités)
    * Créer le contrôleur de l'API REST avec les routes nécessaires (GET, POST, PUT, DELETE)
    * Faire un système d'authentification REST
    * Installer ce bundle pour gérer les CORS : `composer require nelmio/cors-bundle`


## Instructions pour la mise en ligne de l'api sur simplonlyon.fr :

I. Côté local (sur votre machine à vous)
1. S'assurer que ça fonctionne bien comme il faut, sur toutes les pages, en local
2. Faire un commit et un push de tous vos trucs, de préférence sur le gitlab commun
3. Se connecter en ssh à simplonlyon.fr (`ssh username@simplonlyon.fr`)

II.Côté Serveur (sur le terminal connecté en SSH)
1. Aller dans votre dossier www
2. Faire un gitclone de votre projet
3. Renommer le dossier obtenu par le clone en "api" (important, sinon ça ne marchera pas)
4. Aller dans le dossier api
5. Faire un composer install
6. Se connecter au serveur SQL de simplonlyon avec la commande : `mysql -u username -p`               Votre username est votre nom d'utilisateur.ice simplonlyon et votre mot de passe également
7. Créer la base de donnée avec la commande : `CREATE DATABASE username_portfolio;`                Remplacez username par votre nom d'utilisateur.ice simplonlyon
8. Quitter mysql avec la commande : `exit`
9. Créer/modifier un fichier .env avec nano et dedans mettre (au dessus de MYSQL_URL si il y a) :
`MYSQL_HOST=localhost
MYSQL_USER=username
MYSQL_PASSWORD=username
MYSQL_DATABASE=username_portfolio`  
En remplaçant bien à chaque fois username par votre nom d'utilisateur.ice simplonlyon
10. Si vous utiliser webpack Encore etc. il faut faire également un `npm install` et un `npm run build`
11. Essayez d'accéder à votre projet voir si ça marche bien pour toutes les pages/formulaires
12. Si oui, rajouter dans le .env la ligne : `APP_ENV=prod` (mais ça c'est vraiment quand tout tout tout est ok)

III. Mise En ligne du Front
1. Normalement vous avez fait une nouvelle branche sur votre portfolio vous devez donc juste aller dans votre dossier www sur simplonlyon.fr et faire un `git pull` puis un `git checkout ma-branche` sur le nom de la branche que vous avez créée
2. Modifier les url des services pour qu'ils ne pointent pas vers localhost mais sur le lien de votre api sur simplonlyon.fr (ex: https://www.simplonlyon.fr/promo6/username/api/api/)
3. Si vous pouvez faire le `npm run build` en local et commit le résultat du build, c'est un peu mieux que de faire le npm install/build côté serveur